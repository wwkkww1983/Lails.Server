﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Dispatcher;

namespace Lails.Server
{
    public static class HttpConfigurationExtensions
    {
        public static ConcurrentDictionary<string, HttpControllerExtension> Controllers(this HttpConfiguration config)
        {
            ConcurrentDictionary<string, HttpControllerExtension> mapping = new ConcurrentDictionary<string, HttpControllerExtension>(StringComparer.OrdinalIgnoreCase);
            var assemblies = config.Services.GetAssembliesResolver();
            var controllerResolver = config.Services.GetHttpControllerTypeResolver();
            var controllerTypes = controllerResolver.GetControllerTypes(assemblies);
            var controllerLength = DefaultHttpControllerSelector.ControllerSuffix.Length;
            foreach (var objType in controllerTypes)
            {
                var controllerName = objType.Name.Remove(objType.Name.Length - controllerLength);
                MethodInfo[] Methods = objType.GetMethods(BindingFlags.Instance | BindingFlags.Public);
                foreach (MethodInfo Method in Methods)
                {
                    if (Method.GetCustomAttributes(typeof(AliasNameAttribute), false).Length > 0)
                    {
                        var actionName = (ActionNameAttribute)Method.GetCustomAttributes(typeof(ActionNameAttribute), false)?.FirstOrDefault();
                        var action = actionName != null ? actionName.Name : Method.Name;                       
                        var attributes = Method.GetCustomAttributes(typeof(AliasNameAttribute), false);
                        foreach (AliasNameAttribute attribute in attributes)
                        {
                            if (!mapping.ContainsKey(attribute.Alias))
                            {
                                var controllerDescriptor = new HttpControllerDescriptor(config, objType.Name, objType);
                                mapping.TryAdd(attribute.Alias, new HttpControllerExtension() { ControllerDescriptor = controllerDescriptor, Action = action });
                            }

                        }
                    }
                }
            }

            return mapping;
        }
    }
}