
$path="Properties\AssemblyInfo.cs"

$pattern = '\[assembly: AssemblyVersion\("(.*)"\)\]'
(Get-Content $path) | ForEach-Object{
if($_ -match $pattern){
# We have found the matching line
# Edit the version number and put back.
$fileVersion = [version]$matches[1]
$newVersion = "{0}.{1}.{2}.{3}" -f $fileVersion.Major, $fileVersion.Minor, $fileVersion.Build, ($fileVersion.Revision + 1)
'[assembly: AssemblyVersion("{0}")]' -f $newVersion
} else {
# Output line as is
$_
}
} | Set-Content $path  -Encoding utf8

$pattern = '\[assembly: AssemblyFileVersion\("(.*)"\)\]'
(Get-Content $path) | ForEach-Object{
if($_ -match $pattern){
# We have found the matching line
# Edit the version number and put back.
$fileVersion = [version]$matches[1]
$newVersion = "{0}.{1}.{2}.{3}" -f $fileVersion.Major, $fileVersion.Minor, $fileVersion.Build, ($fileVersion.Revision + 1)
'[assembly: AssemblyFileVersion("{0}")]' -f $newVersion
} else {
# Output line as is
$_
}
} | Set-Content $path  -Encoding utf8

nuget pack  -Build -OutputFileNamesWithoutVersion
nuget push -Source "http://nuget.lails.cc/nuget" -ApiKey lails Lails.Server.nupkg