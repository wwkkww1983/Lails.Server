﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace Lails.Server
{
    public static class ControllerExtensions
    {
        public static string GetHeader(this ApiController controller, string name = "appid")
        {
            IEnumerable<string> values = null;
            if (controller.Request.Headers != null && controller.Request.Headers.TryGetValues(name, out values) && values.Count() > 0)
            {
                return values.FirstOrDefault();
            }
            return null;
        }
    }
}
