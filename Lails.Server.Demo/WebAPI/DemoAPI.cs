using Lails.Log;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace Lails.Server.Demo
{
    /// <summary>
    /// DemoAPI
    /// </summary>
    [WebApiResult]
    public class DemoAPIController : ApiController
    {
        string appId;
        Logger log;
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="log"></param>
        /// <param name="appId"></param>
        public DemoAPIController(Logger log, string appId)
        {
            this.appId = appId;
            this.log = log;
        }
        /// <summary>
        /// 获取数据
        /// </summary>
        /// <returns>数据</returns>
        public string GetData()
        {
            return "Lails.Server " + this.appId;
        }
        /// <summary>
        /// 获取对象
        /// </summary>
        /// <param name="id">对象ID</param>
        /// <returns>DemoModel对象</returns>
        public DemoModel GetModel(long id)
        {
            return new DemoModel() { ID = id, Value = "Value" };
        }
        /// <summary>
        /// 获取对象返回异常结果
        /// </summary>
        /// <param name="id">对象ID</param>
        /// <returns>DemoModel对象</returns>
        public DemoModel GetModelOnException(long id)
        {
            throw new CustomException(-1, "无此ID对象", null);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        [AliasName("x.x.x.x")]
        [AliasName("x.x")]
        public string Get(string value)
        {
            var aliasName = this.GetAlias();//读取别名
            return aliasName;
        }
    }

    /// <summary>
    /// DemoModel对象
    /// </summary>
    public class DemoModel
    {
        /// <summary>
        /// 对象ID
        /// </summary>
        public long ID { get; set; }
        /// <summary>
        /// 对象值
        /// </summary>
        public string Value { get; set; }
    }

    /// <summary>
    /// 自定义异常
    /// </summary>
    public class CustomException : Exception
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="code"></param>
        /// <param name="message"></param>
        /// <param name="ex"></param>
        public CustomException(int code, string message, Exception ex)
        : base(message, ex)
        {
            this.HResult = code;
        }
    }
}
